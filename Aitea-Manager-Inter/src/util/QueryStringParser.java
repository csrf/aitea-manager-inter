/**
 *
 */
package util;

/**
 * @author joho
 * クエリストリングの解析を行うクラス
 */
public class QueryStringParser {

	private String query;

	/**
	 * @param query クエリストリング
	 */
	public QueryStringParser(String query){
		this.query = query;
	}

	/**
	 * @return true:クエリストリングがnullである false:クエリストリングがnullでない
	 * クエリストリングがNULL値であることを判定するメソッド
	 */
	public boolean queryIsNull(){
		if(query == null){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param key クエリストリングのキー
	 * @return クエリストリングのバリュー
	 */
	public String get(String key){
		if(query.indexOf(key) == -1) return null;

        int e;
        int s = e = query.indexOf("=", query.indexOf(key)) + 1;
        for(e++; e < query.length(); e++)
            if(query.charAt(e) == '&')
                break;

        return query.substring(s, e);
	}
}
