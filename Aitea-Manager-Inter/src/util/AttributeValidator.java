/**
 *
 */
package util;

import java.util.HashMap;

/**
 * @author joho
 * 文字列の形式について妥当性検証を行うクラス
 */
public class AttributeValidator {

	private HashMap<String, String> regexMap = new HashMap<>();

	/**
	 * 文字列の形式を定義する
	 */
	public AttributeValidator(){
		regexMap.put(AiteaContract.VALIDATE_DATA.EXAM_CATEGORY, "IP|FE|AP|EMPTY");
		regexMap.put(AiteaContract.VALIDATE_DATA.Q_ID, "Q[0-9][0-9][0-9][0-9]");
		regexMap.put(AiteaContract.VALIDATE_DATA.USER_ANSWER, "ア|イ|ウ|エ|？");
		regexMap.put(AiteaContract.VALIDATE_DATA.CORRECT_MISTAKE, "0|1");
		regexMap.put(AiteaContract.VALIDATE_DATA.YEAR, "平成2[0-9]年|EMPTY");
		regexMap.put(AiteaContract.VALIDATE_DATA.TURN, "[春|秋]期|EMPTY");
	}

	/**
	 * @param attr_name 属性の名前 AiteaContractで定義されているものを使う
	 * @param str 検査対象文字列
	 * @return true:定義された形式にマッチ false:定義された形式にアンマッチ
	 */
	public boolean validate(String attr_name, String str){
		if(str == null) return false;
		return str.matches(regexMap.get(attr_name));
	}
}
