package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ServerReceiveBeans;
import dao.DaoFactory;
import dao.InsertServerInfoDao;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.AiteaContract;
import util.AttributeValidator;

/**
 * @author joho
 * 端末から送信された回答情報を受信するサーブレット
 * インターネットへ公開するので値チェックなど厳密にする
 */
@WebServlet("/ServerReceiveServlet")
public class ServerReceiveServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		response.getWriter().append("done");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		JSONArray jsonArray = JSONArray.fromObject(request.getParameter("input"));
		ArrayList<ServerReceiveBeans> beans = new ArrayList<>();
		AttributeValidator v = new AttributeValidator();
		boolean err = false;

		// JSONの受信
		try
		{
			for(int i = 0; i < jsonArray.size(); i++)
			{
				JSONObject jsonobj = (JSONObject)jsonArray.get(i);
				beans.add((ServerReceiveBeans)(JSONObject.toBean(jsonobj, ServerReceiveBeans.class)));

				// 文字化けを直す
				byte bytes[] = ((ServerReceiveBeans)beans.get(i)).getUser_answer().getBytes("ISO-8859-1");
				((ServerReceiveBeans)beans.get(i)).setUser_answer(new String(bytes, "UTF-8"));

				// 入力データチェック
				ServerReceiveBeans bean = (ServerReceiveBeans)beans.get(i);

				if(v.validate(AiteaContract.VALIDATE_DATA.Q_ID, bean.getQuestion_id()) &&
				   v.validate(AiteaContract.VALIDATE_DATA.EXAM_CATEGORY, bean.getExam_category()) &&
				   v.validate(AiteaContract.VALIDATE_DATA.CORRECT_MISTAKE, bean.getCorrect_mistake().toString()) &&
				   v.validate(AiteaContract.VALIDATE_DATA.USER_ANSWER, bean.getUser_answer())){

					// 何もしない
				} else {
					// 不正データ入力
					err = true;
				}
			}

		}
		catch(ClassCastException e)
		{
			// 不正なデータを受け取ったら処理をしない
			response.getWriter().append("posted");
			err = true;
		}
		catch(Exception e)
		{
			// その他の例外も全て処理しない
			response.getWriter().append("posted");
			err = true;
		}
		if(!err)
		{
			InsertServerInfoDao dao = DaoFactory.createInsertServerInfoDao();
			int receive_id_start = dao.examineReceiveId();
			ServerReceiveBeans bean;

			// レコードを追加
			for(Iterator<ServerReceiveBeans> it = beans.iterator(); it.hasNext();
					dao.insertServerReceiveInfo(bean), receive_id_start++)
			{
				bean = (ServerReceiveBeans)it.next();
				bean.setReceive_id(String.format("R%1$010d", new Object[] {
						Integer.valueOf(receive_id_start)
				}));
			}
		}
	}
}
