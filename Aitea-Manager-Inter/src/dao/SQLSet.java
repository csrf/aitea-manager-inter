/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import beans.ServerReceiveBeans;

/**
 * @author joho
 *
 */
public final class SQLSet {

	public static final PreparedStatement insert_ServerReceiveInfo(Connection conn,
			ServerReceiveBeans bean) throws SQLException{
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO tbl_server_receive_info VALUES (?,?,?,?,?,?)";

        try {
			pstmt = conn.prepareStatement(sql);
            // 主キーはnot null
            pstmt.setString(1, bean.getReceive_id());

            // 各項目についてnullチェック
            if(bean.getQuestion_id() == null){
            	pstmt.setNull(2, java.sql.Types.CHAR);
            } else {
            	pstmt.setString(2, bean.getQuestion_id());
            }

            if(bean.getUser_answer() == null){
                pstmt.setNull(3, java.sql.Types.CHAR);
            } else {
                pstmt.setString(3, bean.getUser_answer());
            }

            if(bean.getExam_category() == null){
                pstmt.setNull(4, java.sql.Types.CHAR);
            } else {
                pstmt.setString(4, bean.getExam_category());
            }

            if(bean.getCorrect_mistake() == null){
                pstmt.setNull(5, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(5, bean.getCorrect_mistake().intValue());
            }

            pstmt.setString(6, getCurrentTimeStamp());
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_examinReceiveId(Connection conn) throws SQLException{
		PreparedStatement pstmt = null;
        String sql = "select count(receive_id) as cnt from public.tbl_server_receive_info";

        try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

    private final static String getCurrentTimeStamp()
    {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return format.format(date);
    }
}
