package dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import util.ConfigParser;
import util.OSjudger;


public class DaoFactory
{
    /**
     * @return insert文に関するDAO
     * insert文に関するDAOを返却するメソッド
     */
    public static InsertServerInfoDao createInsertServerInfoDao()
    {
        return new JdbcInsertServerInfoDao(getDataSource());
    }

    private static DataSource getDataSource()
    {
        InitialContext initCon = null;
        DataSource ds = null;
        try
        {
            initCon = new InitialContext();
            ds = (DataSource)initCon.lookup("java:/comp/env/jdbc/postgresql");
        }
        catch(NamingException e)
        {
            e.printStackTrace();
            if(initCon != null)
                try
                {
                    initCon.close();
                }
                catch(NamingException ex)
                {
                    ex.printStackTrace();
                }
        }
        return ds;
    }

    /**
     * @return postgresへのコネクション
     * postgresへのコネクションを返却するメソッド
     */
    public static Connection getConnectionPostgre()
    {
        Connection conn = null;
        ConfigParser configparser = null;

        try{
            Class.forName("org.postgresql.Driver");
        } catch(ClassNotFoundException e) {
            System.out.println("Cannot find the JDBC driver.");
            System.exit(-1);
        }

        try {
        	String filename = null;
        	if(OSjudger.isLinux()){
        		filename = "/usr/share/tomcat/conf/amconf.properties";
        	} else {
        		filename = "C:\\config\\amconf.properties";
        	}
        	configparser = new ConfigParser(filename);
        } catch (FileNotFoundException e) {
        	System.out.println("Cannot find the property file");
        	System.exit(-1);
        } catch (IOException e) {
        	System.out.println("IO error");
        	System.exit(-1);
        }

        String addr = configparser.get("db_addr");
        String port = configparser.get("db_port");
        String name = configparser.get("db_name");
        String user = configparser.get("db_user");
        String pass = configparser.get("db_pass");

        try{
            conn = DriverManager.getConnection(
            		"jdbc:postgresql://" + addr + ":" + port + "/" + name, user, pass);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
}
